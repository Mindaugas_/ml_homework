"""Module contains pre processing related functions
"""
import pandas as pd
import numpy as np
import math
from typing import List
from pandas.api.types import (
    is_string_dtype,
    is_numeric_dtype,
    is_categorical_dtype
)


def convert_strings_to_categories(df: pd.DataFrame) -> pd.DataFrame:
    """Change any columns of strings in a panda's dataframe to a column of
    categorical values.

    Args:
        df (pd.DataFrame): A pandas dataframe.
        Any columns of strings will be changed to categorical values.
    Returns:
        pd.DataFrame: Pandas dataframe with categorical values.
    """
    df_transformed = df.copy()
    for n, c in df_transformed.items():
        if is_string_dtype(c):
            df_transformed[n] = c.astype('category').cat.as_ordered()
    return df_transformed


def get_missing_values_stats(df: pd.DataFrame) -> pd.Series:
    """Computes percentage of missing values in each column.

    Args:
        df (pd.DataFrame): A pandas dataframe.
    Returns:
        pd.Series: Missing values stats.
    """
    return (df.isnull().sum() / len(df)).sort_values(ascending=False)


def add_datepart(df: pd.DataFrame, column_name: str) -> pd.DataFrame:
    """Converts a column of df from a datetime64 to features listed in attr.

    Args:
        df (pd.DataFrame): A pandas dataframe.
    Returns:
        pd.DataFrame: Pandas dataframe with new date type features.
    """
    df_transformed = df.copy()
    fld = df_transformed[column_name]
    attr = ['Month', 'Week', 'Day', 'Dayofweek', 'Dayofyear', 'Hour']
    for n in attr:
        df_transformed[column_name + n] = getattr(fld.dt, n.lower())
    df_transformed.drop(column_name, axis=1, inplace=True)
    return df_transformed


def get_time_related_columns(train_columns: List[str],
                             time_related_column_root: str) -> List[str]:
    """Gets time related column by checking substring.

    Args:
        train_columns (List[str]): All train columns.
        time_related_column_root (str): Time substring.

    Returns:
        List[str]: Time ralted columns.
    """
    time_related_columns = []
    for column in train_columns:
        if time_related_column_root in column:
            time_related_columns.append(column)
    return time_related_columns


def replace_categorical_features_to_numerical_codes(
        df: pd.DataFrame) -> pd.DataFrame:
    """Replaces categorical features to numerical codes.

    Args:
        df (pd.DataFrame): Pandas dataframe with categorical features.

    Returns:
        pd.DataFrame: Pandas dataframe with numerical codes.
    """
    df_transformed = df.copy()
    for n, c in df_transformed.items():
        if is_categorical_dtype(c):
            df_transformed[n] = c.cat.codes + 1
    return df_transformed


def replace_numerical_missing_values_with_medians(
        df: pd.DataFrame) -> pd.DataFrame:
    """Replaces numerical missing values with their medians.

    Args:
        df (pd.DataFrame): Pandas dataframe with numerical features.

    Returns:
        pd.DataFrame: Pandas dataframe without missing numerical values.
    """
    df_transformed = df.copy()
    for n, c in df_transformed.items():
        if is_numeric_dtype(c):
            if pd.isnull(c).sum():
                df_transformed[n] = df_transformed[n].fillna(c.median())
    return df_transformed


def agg_cat_with_target(df: pd.DataFrame, target: str) -> pd.DataFrame:
    """Calculates basic statistics betweeen categorical
    features and target.
    
    Args:
        df (pd.DataFrame): Pandas dataframe with categorical features

        target (str): Target name.
    """
    df_transformed = df.copy()
    for n, c in df.items():
        if is_categorical_dtype(c):
            stats = df_transformed.groupby(n)[target].agg(['mean', 'max', 'min'])
            stats.columns = [f'{n}_{i}' for i in ['mean', 'max', 'min']]
            df_transformed = df_transformed.merge(stats, on=n, how='left')
    return df_transformed

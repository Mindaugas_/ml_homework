"""Module contains model related functions
"""
import numpy as np
import math
from typing import Tuple, Union


def rmse(x: np.array, y: np.array) -> float:
    return math.sqrt(((x-y)**2).mean())


def absolute_mean(x: np.array, y: np.array) -> float:
    return (np.abs(x-y)).mean()


def overpriced_predictions_mean(x: np.array, y: np.array) -> float:
    differences = x - y
    overpriced = differences[differences > 0]
    return overpriced.mean()


def overpriced_predictions_count(x: np.array, y: np.array) -> int:
    differences = x - y
    overpriced = differences[differences > 0]
    return overpriced.count()


def underpriced_predictions_mean(x: np.array, y: np.array) -> float:
    differences = x - y
    underpriced = differences[differences < 0]
    return underpriced.mean()


def underpriced_predictions_count(x: np.array, y: np.array) -> int:
    differences = x - y
    underpriced = differences[differences < 0]
    return underpriced.count()


def get_model_scores(prediction: np.array,
                     actual: np.array) -> Tuple[Union[float, int]]:
    """Calculates model score

    Args:
        prediction (np.array): Predicted values
        actual (np.array): Actual values

    Returns:
        Tuple[float]: Model scores.
    """
    return (rmse(prediction, actual),
            absolute_mean(prediction, actual),
            overpriced_predictions_mean(prediction, actual),
            overpriced_predictions_count(prediction, actual),
            underpriced_predictions_mean(prediction, actual),
            underpriced_predictions_count(prediction, actual),
            )


def get_readable_model_scores(prediction: np.array, actual: np.array) -> str:
    """Gets readable model scores

    Args:
        prediction (np.array): Predicted values
        actual (np.array): Actual values

    Returns:
        str: Readable model scores.
    """
    model_score = get_model_scores(prediction, actual)
    return ('RMSE: {0},\n'
            'absolute mean: {1},\n'
            'overpriced predictions mean: {2}, \n'
            'overpriced predictions count: {3}, \n'
            'underpriced predictions mean: {4}, \n'
            'underpriced predictions count: {5}, \n'.format(*model_score))

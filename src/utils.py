import os
from typing import Dict, Any
import yaml


def create_directory(path: str) -> None:
    """Creates file system directory.

    Args:
        path (str): Directory path.

    Returns:
        None:
    """
    if not os.path.exists(path):
        os.makedirs(path)


def load_yaml_file(file_path: str) -> Dict[str, Any]:
    """Loads yaml file.

    Args:
        file_path (str): Yaml file path.

    Returns:
        Dict[str, Any]: Loaded yaml file.
    """
    with open(file_path, 'r') as stream:
        cfg = yaml.load(stream)
    return cfg


def write_to_file(file_path: str, data: Any) -> None:
    """Writes to file.

    Args:
        file_path (str): File path.
        data (Any): Data you want to write in.

    Returns:
        None:
    """
    with open(file_path, "w+") as file:
        file.write(data)
    file.close()

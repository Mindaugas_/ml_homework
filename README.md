# ML platform
I have created POC of machine learning platform and below project structure is presented
~~~~~~~~~~~~~~~~~~~~~
├── README.md
├── app
│   ├── app.py
│   └── request.py
├── data
│   ├── ml_homework.parquet
│   └── pre_proc_data.parquet
├── experiments
│   └── sale_price
│       ├── config
│       │   ├── experiment_random_forest.yaml
│       │   └── sale_price_prediction_poc.yaml
│       └── pre_proc
│           └── simple_pre_proc.py
├── experiments_result
│   └── sale_price
│       ├── experiment_random_forest
│       │   ├── model_scores_test.txt
│       │   ├── model_scores_train.txt
│       │   └── trained_model.pkl
│       └── sale_price_prediction_poc
│           ├── model_scores_test.txt
│           ├── model_scores_train.txt
│           └── trained_model.pkl
├── homework_ml.md
├── jupyter_notebooks
│   ├── pre_proc_data.ipynb
│   └── sale_price_modeling.ipynb
├── main.py
├── models
│   └── rf_regressor.py
└── src
    ├── models
    │   └── utils.py
    ├── pre_proc
    │   └── utils.py
    └── utils.py
~~~~~~~~~~~~~~~~~~~~~

* Inside the `experiments` folder project folders exist. In this case, `sale_price` project is created.
* Inside project folder `config` and `pre_proc` folders exist.
* `config` folder consists of the experiment configuration files.
* `pre_proc` folder consists of the pre-processing scripts related to the project.

Below info from `sale_price_prediction_poc.yaml` configuration file is presented
~~~~~~~~~~~~~~~~~~~~~
dataset:
    path: experiments/sale_price/pre_proc/simple_pre_proc.py
model:
    path: models/rf_regressor.py
    class_name: RfRegressor
    id: sale_price_prediction_poc
    params:
        n_jobs: -1
        n_estimators: 150
        min_samples_split: 6
        min_samples_leaf: 6
        max_features: sqrt
        max_depth: 25
evaluation:
    target: listing_price_local
    test_size: 0.05
model_output:
    path: experiments_result/sale_price
~~~~~~~~~~~~~~~~~~~~~
* `dataset -> path` - defines pre-processing script which prepares dataset for modelling.
* `model` - defines model path, class, id, params.
* `evaluation` - defines prameters for model evaluation.
* `model_output` - defines model output, where trained model, models score is saved.

# Using ML platform

## Research

Exploration starts with jupyter notebooks. I started with `pre_proc_data.ipynb`, where basic transformations were done on `ml_homework.parquet` dataset and output was saved as `pre_proc_data.parquet`. Then I started ML part with RF in notebook `sale_price_modeling.ipynb`, where I tried several approaches to predict `listing_price_local`. 

## Development
After the exploration step, several ideas can be developed using ML platform. Development starts by creating experiment config files and running these experiments using main modules `python main.py`, where you need to specify path to the configuration file.

### Running the experiment
* `python main.py`

## Deployment
The trained machine learning model is deployed on Flask server. All scripts related to model deployment is stored at `app` folder. Before running the app you need to specify manually `modelfile` in `app/app.py` which is path to trained model and define `data` in  `request.py`.

### Running the app
* Firstly run the server `python app/app.py`
* Secondly run the request `python app/request.py`

import requests
import json

url = 'http://0.0.0.0:5000/price/'

# 'size', 'catalog_code_4', 'catalog_code_2',
# 'window_items_sold', 'total_gmv_sold', 'package_size_code',
# 'total_items_listed', 'brand', 'catalog_code_3',
# 'window_gmv_sold', 'window_items_listed'
data = [[0.0, 51.0, 166.0, 1115.79,
         0.0, 330.0, 26.0, 4.0, 177.0, 241.0, 220.0]]
j_data = json.dumps(data)
headers = {'content-type': 'application/json', 'Accept-Charset': 'UTF-8'}
r = requests.post(url, data=j_data, headers=headers)
print(r, r.text)

from flask import Flask, request, redirect, url_for, flash, jsonify
import numpy as np
import pickle as p
import json


app = Flask(__name__)


@app.route('/price/', methods=['POST'])
def makecalc():
    data = request.get_json()
    prediction = np.array2string(model.predict(data))

    return jsonify(prediction)

if __name__ == '__main__':
    modelfile = ('experiments_result/sale_price/'
                 'sale_price_prediction_poc/trained_model.pkl')
    model = p.load(open(modelfile, 'rb'))
    app.run(debug=True, host='0.0.0.0')

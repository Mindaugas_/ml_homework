# ML Engineer Homework Assignment

## Code Philosophy

At Vinted, we strive to write clean, simple, and testable code that is easy to maintain. We highly value consistency and following a [language code style](https://www.python.org/dev/peps/pep-0008/) and expect to see the same values conveyed in problem solution.

## Problem

Vinted members upload listings of items that they want to sell. We want to help out the sellers to list their items faster by suggesting a price for which they could sell their items. Your objective is to predict the price point at which we expect the sale to take place. Moreover, some listed items have a low probability of being sold. Your second objective is to predict if the item will be sold and how soon. After you complete your first two objectives, you should have one or two models. The third objective is to prepare the models for inference by creating a web service (preferably RESTful) with the models. You can use any tool that you see fit for this task, such as Django, Flask, etc. Vinted has a lot of members, so we care a lot about inference speed, model size, and reliability of your deployment solution.

## Data

The dataset is available here: [https://vinted-ml-homework.s3.eu-central-1.amazonaws.com/OCJ4HAtw0xW/ml_homework.parquet](https://vinted-ml-homework.s3.eu-central-1.amazonaws.com/OCJ4HAtw0xW/ml_homework.parquet)

You will be provided with `ml_homework.parquet` file containing the data about our listings. The names of the columns should give a hint about what they represent.

## Requirements

- Use Python 3 in this project. This requirement doesn't apply to libraries written in a different language.
- Do an exploratory data analysis of the provided dataset. We suggest using Jupyter notebooks or similar.
- Build a machine learning model that predicts the sale price of the item. The seller entered the price is in the `listing_price_local` column.
- Build a machine learning model that predicts the success of the listed item. We provide the upload time in the `local_time` column and the sale time in the `sale_time` column.
- Build a web service with `/price` and `/sale` endpoints that accepts a JSON request with some or all of the fields present in the `ml_homework.parquet` dataset and responds with a JSON with the probabilities.

## Suggestions

- Your solution should match the code philosophy described above.
- There should be an easy way to run the solution. README is a perfect place to provide instructions.
- The solution should be local, i.e., it should not make network requests when making the inference (predicting).
- A short documentation of design decisions and assumptions can be provided in the code itself or in the README.
- Time is not constrained, feel free to solve it whenever you're able to. Just don't forget to communicate with us if you can't find a free evening within a couple of weeks :-)

## Evaluation Criteria

Your solution will be evaluated based on:

- The demonstration of your data analysis skills.
- The demonstration of your machine learning skills.
- Your knowledge of software engineering best practices.
- The RMSE of the price model tested on a different dataset.
- The relevant metrics of the sale model - it depends on what type of model will you choose to use.
- The inference speed of your models.

"""Run an experiment."""

import logging
import sys
import os
import imp
import pprint
import pandas as pd
from sklearn.model_selection import train_test_split

from src.utils import load_yaml_file, write_to_file

logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',
                    level=logging.DEBUG,
                    stream=sys.stdout)

experiment_config_path = ('experiments/sale_price/config'
                          '/sale_price_prediction_poc.yaml')

logging.info(f"Loading experiment config file: {experiment_config_path}")
cfg = load_yaml_file(experiment_config_path)
logging.debug(f"Experiment file content {cfg}")

logging.info(f"Loading dataset module: {cfg['dataset']['path']}")
pre_proc_module = imp.load_source('pre_proc_module', cfg['dataset']['path'])

logging.info(f"Loading actual dataset")
df = pre_proc_module.run()

logging.info(f"Loading model module: {cfg['model']['path']}")
model_module = imp.load_source('model_module', cfg['model']['path'])

logging.info(f"Loading class {cfg['model']['class_name']}")
model_class = getattr(model_module, cfg['model']['class_name'])

logging.info(f"Loading model parameters")
params = cfg['model']['params']
logging.debug(f"Model params: {params}")


logging.info(f"Loading model id")
model_id = cfg['model']['id']
logging.debug(f"Model id: {model_id}")

logging.info(f"Loading model output_dir")
model_output_dir = os.path.join(cfg['model_output']['path'], model_id)
logging.debug(f"Model output dir: {model_output_dir}")

logging.info(f"Model initialization")
model = model_class(model_id=model_id,
                    params=params,
                    model_output_dir=model_output_dir)

logging.info(f"Loading target name")
target = cfg['evaluation']['target']
logging.debug(f"Target: {target}")


logging.info(f"Loading test_size")
test_size = cfg['evaluation']['test_size']
logging.debug(f"Test size: {test_size}")


train_columns = list(set(df.columns) - set([target]))

logging.info(f"Splitting dataset into train and test.")
X_train, X_test, y_train, y_test = train_test_split(
    df[train_columns],
    df[target],
    test_size=test_size)

logging.info("Model training started")
logging.debug(f"X_train - {X_train.shape},"
              f"X_test - {X_test.shape}.")
model.fit(X_train, y_train)
logging.info("Model training finished")

logging.info(f"Saving trained model")
model.save_model()

logging.info(f"Evaluating trained model on train dataset")
model_scores_train = model.evaluate_model(model.predict(X_train), y_train)
logging.debug(model_scores_train)
logging.info(f"Write score to model output dir")
write_to_file(os.path.join(model_output_dir, 'model_scores_train.txt'),
              model_scores_train)

logging.info(f"Evaluating trained model on test dataset")
model_scores_test = model.evaluate_model(model.predict(X_test), y_test)
logging.debug(model_scores_test)
logging.info(f"Write score to model output dir")
write_to_file(os.path.join(model_output_dir, 'model_scores_test.txt'),
              model_scores_test)

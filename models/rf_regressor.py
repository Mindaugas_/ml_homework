from sklearn.ensemble import RandomForestRegressor
import pickle
import os
import pandas as pd
import numpy as np
from typing import Dict, Any, Tuple, Union

from src.utils import create_directory
from src.models.utils import get_readable_model_scores


class RfRegressor:
    def __init__(self,
                 model_id: str,
                 params: Dict[str, Any],
                 model_output_dir: str):
        self.model_id = model_id
        self.model = RandomForestRegressor(**params)
        self.model_output_dir = model_output_dir
        create_directory(self.model_output_dir)

    def fit(self, X: pd.DataFrame, y: pd.Series) -> None:
        self.model.fit(X, y)

    def predict(self, X: pd.DataFrame) -> None:
        return self.model.predict(X)

    def save_model(self) -> None:
        pickle.dump(
            self.model,
            open(os.path.join(self.model_output_dir,
                              'trained_model.pkl'), 'wb'))

    def evaluate_model(self,
                       predicted_values: np.array,
                       actual_values: np.array) -> Tuple[Union[str, int]]:
        return get_readable_model_scores(predicted_values, actual_values)

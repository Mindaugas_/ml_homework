"""This module contains run function where basic pre processing is done
for the sale_price prediction task.
"""

import src.pre_proc.utils as pre_proc
import pandas as pd


def run() -> pd.DataFrame:
    df = pd.read_parquet('data/pre_proc_data.parquet', engine='pyarrow')
    df = pre_proc.convert_strings_to_categories(df)
    # Removing outliers
    df = df[df.listing_price_local < 150]
    # Selecting most important features
    columns = ['package_size_code',
               'brand',
               'window_gmv_sold',
               'window_items_listed',
               'catalog_code_2',
               'catalog_code_3',
               'size',
               'catalog_code_4',
               'window_items_sold',
               'total_gmv_sold',
               'total_items_listed',
               'listing_price_local'
               ]

    df = pre_proc.replace_categorical_features_to_numerical_codes(df[columns])
    df = pre_proc.replace_numerical_missing_values_with_medians(df)

    return df
